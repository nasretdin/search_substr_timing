import datetime
import re
import sys

def timeit(method):
    def timed(*args, **kwargs):
        print('...............')
        ts = time.time()
        result = method(*args, **kwargs)
        te = time.time()
        if 'log_time' in kwargs:
            name = kwargs.get('log_name', method.__name__.upper())
            kwargs['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.22f ms' % (method.__name__, (te - ts) * 1000))
        return result

    return timed
    
def timer(func):
    def wrapper(*args, **kwargs):
        print('...............')
        print(func.__name__)
        start = datetime.datetime.now().time().microsecond
        func(*args, **kwargs)
        print(f'{datetime.datetime.now().time().microsecond - start} ms\n')

    return wrapper


# =====================================
NO_OF_CHARS = 256


def badCharHeuristic(string, size):
    """
    The preprocessing function for
    Boyer Moore's bad character heuristic
    """

    # Initialize all occurrence as -1
    badChar = [-1] * NO_OF_CHARS

    # Fill the actual value of last occurrence
    for i in range(size):
        badChar[ord(string[i])] = i

    # return initialized list
    return badChar


@timer
def boyer_moor(txt, pat):
    """
    A pattern searching function that uses Bad Character
    Heuristic of Boyer Moore Algorithm
    """
    m = len(pat)
    n = len(txt)

    # create the bad character list by calling
    # the preprocessing function badCharHeuristic()
    # for given pattern
    badChar = badCharHeuristic(pat, m)

    # s is shift of the pattern with respect to text
    s = 0
    while (s <= n - m):
        j = m - 1

        # Keep reducing index j of pattern while
        # characters of pattern and text are matching
        # at this shift s
        while j >= 0 and pat[j] == txt[s + j]:
            j -= 1

        # If the pattern is present at current shift,
        # then index j will become -1 after the above loop
        if j < 0:
            print(f"found at {s}")

            '''    
                Shift the pattern so that the next character in text
                      aligns with the last occurrence of it in pattern.
                The condition s+m < n is necessary for the case when
                   pattern occurs at the end of text
               '''
            s += (m - badChar[ord(txt[s + m])] if s + m < n else 1)
        else:
            '''
               Shift the pattern so that the bad character in text
               aligns with the last occurrence of it in pattern. The
               max function is used to make sure that we get a positive
               shift. We may get a negative shift if the last occurrence
               of bad character in pattern is on the right side of the
               current character.
            '''
            s += max(1, j - badChar[ord(txt[s + j])])


# =====================================


# =======================================

def prefix(s):
    P = [0] * len(s)
    j = 0
    i = 1

    while i < len(s):
        if s[j] != s[i]:
            if j > 0:
                j = P[j - 1]
            else:  # j == 0
                i += 1
        else:  # s[j] == s[i]
            P[i] = j + 1
            i += 1
            j += 1

    return P


def kmp(sub: str, s: str):
    """Search substring in string Knuth, Morris, Prath

    Arguments:
        sub {str} -- substring
        s {str} -- string
    """
    k = 0
    length = 0
    P = prefix(sub)

    while k < len(s):
        if sub[length] == s[k]:
            k += 1
            length += 1

            if length == len(sub):
                return k - len(sub)

        elif length > 0:
            length = P[length - 1]
        else:
            k += 1

    return -1


@timer
def kmp_search(_pattern, _string):
    _lsub = len(_pattern)
    _index = kmp(_pattern, _string)
    print(f"found at {_index}")


@timer
def naive_search(_pattern, _string):
    pat_len = len(_pattern)
    i = 0
    while i < len(_string):
        tested_substr = _string[i:pat_len + i]
        if tested_substr == _pattern:
            print(f'found at {i}')
            return True
        i += 1
    return False


@timer
def just_in_check(_pattern, _string):
    if pattern in _string:
        print("found")
        return True
    return False


@timer
def re_search(_pattern, _string):
    search_result = re.search(rf'{_pattern}', _string)
    if search_result:
        print(f"found at {search_result.regs[0][0]}")
        return True
    return False


@timer
def use_find(_pattern, _string):
    result = _string.find(_pattern)
    if result > 0:
        print(f"found at {result}")
        return True
    return False


@timer
def use_index(_pattern, _string):
    try:
        result = _string.index(_pattern)
    except ValueError:
        result = -1
        pass
    if result > 0:
        print(f"found at {result}")
        return True
    return False


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('pass "l" for long os "s" for short string.')
        # # ## 140
        new_string = 'Donations towards electricity bill and developer beer fund can be sent via Bitcoin to 18euSAZztpZ9wcN6xZS3vtNnE1azf8niDk try add.'
        pattern = '18euSAZztpZ9wcN6xZS3vtNnE1azf8niD'

    elif sys.argv[1] == 'l':
        # ## 8000
        new_string = '#!/bin/sh# shellcheck disable=SC2030,SC2031# SC2030: Modification of WINE is local (to subshell caused by (..) group).# SC2031: WINE was modified in a subshell. That change might be lost# This has to be right after the shebang, see: https://github.com/koalaman/shellcheck/issues/779# Name of this version of winetricks (YYYYMMDD)# (This doesn\t change often, use the sha256sum of the file when reporting problems)WINETRICKS_VERSION=20240105-next# This is a UTF-8 file# You should see an o with two dots over it here [ö]# You should see a micro (u with a tail) here [µ]# You should see a trademark symbol here [™]#--------------------------------------------------------------------## Winetricks is a package manager for Win32 dlls and applications on POSIX.# Features:# - Consists of a single shell script - no installation required# - Downloads packages automatically from original trusted sources# - Points out and works around known wine bugs automatically# - Both command-line and GUI operation# - Can install many packages in silent (unattended) mode# - Multiplatform; written for Linux, but supports OS X and Cygwin too## Uses the following non-POSIX system tools:# - wine is used to execute Win32 apps except on Cygwin.# - ar, cabextract, unrar, unzip, and 7z are needed by some verbs.# - aria2c, wget, curl, or fetch is needed for downloading.# - fuseiso, archivemount (Linux), or hdiutil (macOS) is used to mount .iso images.# - perl is used for displaying download progress for wget when using zenity# - pkexec, sudo, or kdesu (gksu/gksudo/kdesudo are deprecated upstream but also still supported)#   are used to mount .iso images if the user cached them with -k option.# - sha256sum, sha256, or shasum (OSX 10.5 does not support these, 10.6+ is required)# - torify is used with option \--torify\ if sites are blocked in single countries.# - xdg-open (if present) or open (for OS X) is used to open download pages#   for the user when downloads cannot be fully automated.# - xz is used by some verbs to decompress tar archives.# - zenity is needed by the GUI, though it can limp along somewhat with kdialog/xmessage.## On Ubuntu (20.04 and newer), the following line can be used to install all the prerequisites:#    sudo apt install aria2 binutils cabextract fuseiso p7zip-full pkexec tor unrar unzip wine xdg-utils xz-utils zenity## On older Ubuntu, the following line can be used to install all the prerequisites:#    sudo apt install aria2 binutils cabextract fuseiso p7zip-full policykit-1 tor unrar unzip wine xdg-utils xz-utils zenity## On Fedora, these commands can be used (RPM Fusion is used to install unrar):#    sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm#    sudo dnf install binutils cabextract fuseiso p7zip-plugins polkit tor unrar unzip wget wine xdg-utils xz zenity## See https://github.com/Winetricks/winetricks for documentation and tutorials,# including how to contribute changes to winetricks.##--------------------------------------------------------------------## Copyright:#   Copyright (C) 2007-2014 Dan Kegel <dank!kegel.com>#   Copyright (C) 2008-2024 Austin English <austinenglish!gmail.com>#   Copyright (C) 2010-2011 Phil Blankenship <phillip.e.blankenship!gmail.com>#   Copyright (C) 2010-2015 Shannon VanWagner <shannon.vanwagner!gmail.com>#   Copyright (C) 2010 Belhorma Bendebiche <amro256!gmail.com>#   Copyright (C) 2010 Eleazar Galano <eg.galano!gmail.com>#   Copyright (C) 2010 Travis Athougies <iammisc!gmail.com>#   Copyright (C) 2010 Andrew Nguyen#   Copyright (C) 2010 Detlef Riekenberg#   Copyright (C) 2010 Maarten Lankhorst#   Copyright (C) 2010 Rico Schüller#   Copyright (C) 2011 Scott Jackson <sjackson2!gmx.com>#   Copyright (C) 2011 Trevor Johnson#   Copyright (C) 2011 Franco Junio#   Copyright (C) 2011 Craig Sanders#   Copyright (C) 2011 Matthew Bauer <mjbauer95!gmail.com>#   Copyright (C) 2011 Giuseppe Dia#   Copyright (C) 2011 Łukasz Wojniłowicz#   Copyright (C) 2011 Matthew Bozarth#   Copyright (C) 2013-2017 Andrey Gusev <andrey.goosev!gmail.com>#   Copyright (C) 2013-2020 Hillwood Yang <hillwood!opensuse.org>#   Copyright (C) 2013,2016 André Hentschel <nerv!dawncrow.de>#   Copyright (C) 2023 Georgi Georgiev (RacerBG) <g.georgiev.shumen!gmail.com>## License:#   This program is free software; you can redistribute it and/or#   modify it under the terms of the GNU Lesser General Public#   License as published by the Free Software Foundation; either#   version 2.1 of the License, or (at your option) any later#   version.##   This program is distributed in the hope that it will be useful,#   but WITHOUT ANY WARRANTY; without even the implied warranty of#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the#   GNU Lesser General Public License for more details.##   You should have received a copy of the GNU Lesser General Public#   License along with this program.  If not, see#   <https://www.gnu.org/licenses/>.##--------------------------------------------------------------------# Coding standards:## Portability:# - Portability matters, as this script is run on many operating systems# - No bash, zsh, or csh extensions; only use features from#   the POSIX standard shell and utilities; see#   https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html# - Prefer classic sh idioms as described in e.g.#   \Portable Shell Programming\ by Bruce Blinn, ISBN: 0-13-451494-7# - If there is no universally available program for a needed function,#   support the two most frequently available programs.#   e.g. fall back to wget if curl is not available; likewise, support#   both sha256sum and sha256.# - When using Unix commands like cp, put options before filenames so it will#   work on systems like OS X.  e.g. \rm -f foo.dat\, not \rm foo.dat -f\## Formatting:# - Your terminal and editor must be configured for UTF-8#   If you do not see an o with two dots over it here [ö], stop!# - Do not use tabs in this file or any verbs.# - Indent 4 spaces.# - Try to keep line length below 80 (makes printing easier)# - Open curly braces (\{\),#   then should go on the same line as \if/elif\#   close curlies (\}\) and \fi\ should line up with the matching { or if,#   cases indented 4 spaces from \case\ and \esac\.  For instance,##      if test \$FOO\ = \bar\; then#         echo \FOO is bar\#      fi##      case \$FOO\ in#          bar) echo \FOO is still bar\ ;;#      esac## Commenting:# - Comments should explain intent in English# - Keep functions short and well named to reduce need for comments## Naming:# Public things defined by this script, for use by verbs:# - Variables have uppercase names starting with W_# - Functions have lowercase names starting with w_## Private things internal to this script, not for use by verbs:# - Local variables have lowercase names starting with uppercase _W_#   (and should not use the local declaration, as it is not POSIX)# - Global variables have uppercase names starting with WINETRICKS_# - Functions have lowercase names starting with winetricks_# FIXME: A few verbs still use winetricks-private functions or variables.## Internationalization / localization:# - Important or frequently used message should be internationalized#   so translations can be easily added.  For example:#     case $LANG in#         de*) echo \Das ist die deutsche Meldung\ ;;#         *)   echo \This is the English message\ ;;#     esac## Support:# - Winetricks is maintained by Austin English <austinenglish!$gmail.com>.# - If winetricks has helped you out, then please consider donating to the FSF/EFF as a thank you:#   * EFF - https://supporters.eff.org/donate/button#   * FSF - https://my.fsf.org/donate# - Donations towards electricity bill and developer beer fund can be sent via Bitcoin to 18euSAZztpZ9wcN6xZS3vtNnE1azf8niDk# - 10b092199315ff4574d09ff3e7686815h try add new row'
        pattern = '18euSAZztpZ9wcN6xZS3vtNnE1azf8niD'
        # pattern = '18euSAZztpZtwcuyN6xZS3vtNnE1azf8niD'  # not found pattern
        # pattern = '18euSAZztp'

    elif sys.argv[1] == 's':
        # # # ## 600
        new_string = 'A few verbs still use winetricks-private functions or #  swing   case $LANG in#         de*) echo \Das ist die deutsche Meldung\ ;;#         *)   echo \This is the English message\ ;;#     esac## Support:# - Winetricks is maintained by Austin English <austinenglish!$gmail.com>.# - If winetricks has helped you out, then please consider donating to the FSF/EFF as a thank you:#   * EFF - https://supporters.eff.org/donate/button#   * FSF - https://my.fsf.org/donate# - Donations towards electricity bill and developer beer fund can be sent via Bitcoin to 18euSAZztpZ9wcN6xZS3vtNnE1azf8niDk to try add'
        pattern = '18euSAZztpZ9wcN6xZS3vtNnE1azf8niD'
        # #pattern = '18euSAZztp'
    else:
        print("Unknown arg.")
        print('pass "l" for long os "s" for short string.')
        sys.exit()

    use_find(pattern, new_string)
    use_index(pattern, new_string)
    just_in_check(pattern, new_string)
    naive_search(pattern, new_string)
    re_search(pattern, new_string)
    kmp_search(pattern, new_string)
    boyer_moor(new_string, pattern)
